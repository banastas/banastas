<a href="https://banastas.photo">banastas.photo</a> | <a href="https://banastas.dev">banastas.dev</a> | <a href="https://banastas.blog">banastas.blog</a> | <a href="https://banastas.crypto">banastas.crypto</a>

### banastas.blog

  * [Photography Workflow 2022](https://banastas.blog/photo-workflow-2022.html)
  * [Repeat Recipes: Poulet au citron](https://banastas.blog/poulet-au-citron.html)
  * [Repeat Recipes: Ropa Vieja](https://banastas.blog/ropa-vieja.html)
  * [4 App Home Screen](https://banastas.blog/4-app-home-screen.html)
  * [Side Projects: Where/When](https://banastas.blog/wherewhen.html)
